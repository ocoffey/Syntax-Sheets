# Syntax Sheets for quickly picking up language syntax

Suggesting that the user has prior experience in one language's syntax.

---

## [C](https://gitlab.com/ocoffey/Syntax-Sheets/-/tree/master/C "C Syntax Sheets")

## [C++](https://gitlab.com/ocoffey/Syntax-Sheets/-/tree/master/C%2B%2B "C++ Syntax Sheets")

## [Javascript](https://gitlab.com/ocoffey/Syntax-Sheets/-/tree/master/Javascript "Javascript Syntax Sheets")

## [Python](https://gitlab.com/ocoffey/Syntax-Sheets/-/tree/master/Python "Python Syntax Sheets")

## [Ruby](https://gitlab.com/ocoffey/Syntax-Sheets/-/tree/master/Ruby "Ruby Syntax Sheets")

## [Rust](https://gitlab.com/ocoffey/Syntax-Sheets/-/tree/master/Rust "Rust Syntax Sheets")

## [Command Line Git Walkthrough/Github setup](https://gitlab.com/ocoffey/Syntax-Sheets/-/blob/master/Git/README.md "Git Walkthrough")
