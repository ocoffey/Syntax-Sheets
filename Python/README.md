# Syntax Sheets by Section

Syntax Sheets for the Python programming language, sorted by section.

## 1. [Introduction](https://gitlab.com/ocoffey/Syntax-Sheets/-/blob/master/Python/1_Intro.md "Intro")

An intro to python's printing and comments.

## 2. [Data Types](https://gitlab.com/ocoffey/Syntax-Sheets/-/blob/master/Python/2_Data_Types.md "Data Types")

Simple Data Types, and how to declare variables of those types.

## 3. [User Interaction](https://gitlab.com/ocoffey/Syntax-Sheets/-/blob/master/Python/3_User_Interaction.md "User Interaction")

A further look at printing to console, and taking user input.

## 4. [Operators](https://gitlab.com/ocoffey/Syntax-Sheets/-/blob/master/Python/4_Operators.md "Operators")

Mathematical and Logical Operators that are used within conditional statements and for manipulations.

## 5. [Control Flow](https://gitlab.com/ocoffey/Syntax-Sheets/-/blob/master/Python/5_Control_Flow.md "Control Flow")

A look at basic conditional statements and loops.

## 6. [Complex Data Types](https://gitlab.com/ocoffey/Syntax-Sheets/-/blob/master/Python/6_Complex_Data_Types.md "Complex Data Types")

A brief look at lists.

## 7. [Functions](https://gitlab.com/ocoffey/Syntax-Sheets/-/blob/master/Python/7_Functions.md "Functions")

A look into user defined (helper) functions, and recursive functions.
