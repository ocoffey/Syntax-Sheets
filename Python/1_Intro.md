# Introduction to Python Language Syntax

An introduction to the basics of Python. Python scripts use the `.py` file type.

---

## Printing to the console

In Python, data is typically printed to the screen via the `print()` function. The things you want to print go inside the parenthesis.

```python
print("ok computer")
print(55)
```

## Comments

Comments are used in code to help describe what sections are doing, and can also be used to temporarily deactivate sections of code.

### Single-Line Comments

Single-line comments in python are preceded with the `#` symbol.

```python
# This next section of code will print 'hello!' to the user
print("Hello!")
```

### Multi-Line Comments

Multi-line comments in python are actually a type of string (which is covered later). They have three `"` before and after your words.

```python
"""
This is a multi-line comment.
You can see this, because it takes up multiple lines!
This next section will print a small statement.
"""
print("The was a lot of comments")
```

[Next](https://gitlab.com/ocoffey/Syntax-Sheets/-/blob/master/Python/2_Variables.md "Variables"), we need to about containers for storing data (variables).
