# Syntax Sheets by Section

Syntax Sheets for the Javascript programming language, sorted by section.

## 1. [Introduction](https://github.com/ocoffey/Syntax-Sheets/blob/master/Javscript/1_Intro.md "Intro")

An explanation of user input and output, in addition to comments.
